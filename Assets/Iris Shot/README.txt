Iris Shot - Simple Onscreen Spotlight Effect

This asset has several application options. What you can do with it on screen:
 — spotlight effect to focus attention on specific screen point
 — cinematic black borders on top and bottom (or from left and right if you want it)
 — fullscreen fade in/out effect
 — transition effect like in old cartoons!

NOTE:
1) Here's no tweener included. It means that you can't do runtime things with only this package. You need to write your own script that will change the values in time or/and use other tools (for example, DoTween, which I used to create this demo video).
2) Other screen effects that use the Graphics.Blit method can overlap the Iris Shot changes — watch out for the render order.

Features at the moment:
 + Fully opened and commented code
 + Lightweight enough - optimal for mobile

How to use:
 1. Just attach IrisShotController to GameObject with Main Camera on it
 2. Use the IrisShotController properties to control effect