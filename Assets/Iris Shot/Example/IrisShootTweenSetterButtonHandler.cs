﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(Button))]
public class IrisShootTweenSetterButtonHandler : MonoBehaviour
{
    public SimpleIrisShotTweener tweener;

    [SerializeField]
    private IrisShotTween tween;

    private Button button;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(onClick);
    }

    private void onClick()
    {
        tweener.SetTween(tween);
    }

}
