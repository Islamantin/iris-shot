﻿using System.IO;
using UnityEditor;
using UnityEngine;

// main script to control iris shot effect
// needs PostEffector to get/set params from target material
[ExecuteInEditMode]
[RequireComponent(typeof(PostEffector))]
public class IrisShotController : MonoBehaviour
{
    private PostEffector postEffector;

    [Range(0f, 1f)]
    [SerializeField]
    private float alpha = 0f; // intencity of blackness outside the shape

    [Range(0f, 1f)]
    [SerializeField]
    private float scale = 0.2f; // scale of shape on screen

    [SerializeField]
    private Vector2 position = new Vector2(0.5f, 0.5f); // position of shape on screen

    public bool autoSizeProportionCorrection = true; // you'll need it true only if you frequently changing size of game screen (in editor, for example)
    [SerializeField]
    private Vector2 sizeProportion; // value of shape size proportion relative to screen size
    // set it (0, 1) to get cinematic borders

    // camera on this GameObject
    private Camera Camera { get { return GetComponent<Camera>(); } }

    void Awake()
    {
        postEffector = gameObject.GetComponent<PostEffector>();
        // everything necessary is done automatically for you when you attach this script to gameobject
#if UNITY_EDITOR
        var cam = Camera;
        if (cam != null && cam.Equals(Camera.main))
        {
            // canvases needs to be not in 'Screen Space - Overlay' mode, because of rendering order 
            // (ui elements are draws over)
            // and here all the canvases switches on Camera mode
            //var canvases = FindObjectsOfType<Canvas>();
            //foreach (var c in canvases)
            //{
            //    if (c.renderMode == RenderMode.ScreenSpaceOverlay)
            //    {
            //        c.renderMode = RenderMode.ScreenSpaceCamera;
            //        c.worldCamera = cam;
            //        Debug.LogWarning("Renderer mode of canvas \"" + c.gameObject.name
            //            + "\" was changed from \'Screen Space - Overlay\' to \'Screen Space - Camera\'.");
            //    }
            //}

            // you don't need to set necessary material by yourself
            if (postEffector.material == null)
            {
                // it just finds material in assets
                var path = AssetDatabase.GUIDToAssetPath(AssetDatabase.FindAssets("IrisShotMaterial l:IrisShot t:Material")[0]);
                postEffector.material = AssetDatabase.LoadAssetAtPath<Material>(path);
            }
        }
        else
        {
            GameObject.DestroyImmediate(this);
        }
#endif
        // getting values from material
        MapValuesFromMaterial();
        // subscribing to event which invokes when image size will change 
        postEffector.TextureSizeChanged += DoSizeProportionCorrection;
    }

    // invokes when any value of this script changed in inspector
    // just immediate applies new values to material in editor
    private void OnValidate()
    {
        if (postEffector != null)
        {
            Alpha = alpha;
            Scale = scale;
            Position = position;
            if (!autoSizeProportionCorrection)
                Proportion = sizeProportion;
            else DoSizeProportionCorrection();
        }
    }

    // calculating quad proportion of shape size 
    public Vector2 CalculateCorrectSizeProportion(Vector2 textureSize)
    {
        var r = new Vector2(textureSize.x, textureSize.y);
        if (r.x > r.y)
        {
            r = new Vector2(1f, r.y / r.x);
        }
        else
        {
            r = new Vector2(r.x / r.y, 1f);
        }
        return r;
    }

    // get and set position of shape
    public Vector2 Position
    {
        get { return position; }
        set
        {
            position = value;
            postEffector.material.SetFloat("_CenterX", position.x);
            postEffector.material.SetFloat("_CenterY", position.y);
        }
    }

    // get and set scale of shape
    public float Scale
    {
        get { return scale; }
        set
        {
            scale = value;
            postEffector.material.SetFloat("_Scale", scale * 1.3f);
        }
    }

    // get and set blackness intencity outside the shape
    public float Alpha
    {
        get { return alpha; }
        set
        {
            alpha = value;
            postEffector.material.SetFloat("_Alpha", alpha);
        }
    }

    // get and set size proportion of shape
    public Vector2 Proportion
    {
        get { return sizeProportion; }
        set
        {
            sizeProportion = value;
            postEffector.material.SetFloat("_HorizontalProp", sizeProportion.x);
            postEffector.material.SetFloat("_VerticalProp", sizeProportion.y);
        }
    }


    // setting position of shape to screen center
    [ContextMenu("Center Position")]
    public void CenterPosition()
    {
        position = new Vector2(0.5f, 0.5f);
        Position = position;
    }

    // set shape position on point of some transform
    public void FocusOnTransform(Transform t)
    {
        var p = t.position;
        var sp = Camera.WorldToScreenPoint(p);
        Position = new Vector2(sp.x / Camera.pixelWidth, sp.y / Camera.pixelHeight);
    }

    // manual size proportion correction when clicking it in context menu of component in inspector
    [ContextMenu("Do Ratio Correction")]
    public void DoSizeProportionCorrection()
    {
        sizeProportion = CalculateCorrectSizeProportion(postEffector.TextureSize);
        Proportion = sizeProportion;
    }

    // auto size proportion correction - event handler
    private void DoSizeProportionCorrection(object sender, TextureSizeChangedEventArgs e)
    {
        if (autoSizeProportionCorrection)
        {
            sizeProportion = CalculateCorrectSizeProportion(e.NewTextureSize);
            Proportion = sizeProportion;
        }
    }

    // getting parameter values from material parameter values
    // can be invoked from component context menu in inspector
    [ContextMenu("Map Values From Material")]
    public void MapValuesFromMaterial()
    {
        alpha = postEffector.material.GetFloat("_Alpha");
        scale = postEffector.material.GetFloat("_Scale") / 1.3f;
        var cx = postEffector.material.GetFloat("_CenterX");
        var cy = postEffector.material.GetFloat("_CenterY");
        position = new Vector2(cx, cy);
        var hp = postEffector.material.GetFloat("_HorizontalProp");
        var vp = postEffector.material.GetFloat("_VerticalProp");
        sizeProportion = new Vector2(hp, vp);
    }

    [ContextMenu("Make Tween")]
    public void MakeTween()
    {
        var asset = ScriptableObject.CreateInstance<IrisShotTween>();
        asset.alpha = AnimationCurve.EaseInOut(0f, 0f, 1f, Alpha);
        asset.scale = AnimationCurve.EaseInOut(0f, 0f, 1f, Scale);
        asset.positionX = AnimationCurve.Constant(0f, 1f, Position.x);
        asset.positionY = AnimationCurve.Constant(0f, 1f, Position.y);
        asset.sizeProportionX = AnimationCurve.Constant(0f, 1f, Proportion.x);
        asset.sizeProportionY = AnimationCurve.Constant(0f, 1f, Proportion.y);
        var folder = "Assets/Iris Shot/Tweens/";
        if (!AssetDatabase.IsValidFolder(folder))
            Directory.CreateDirectory(Path.GetDirectoryName(folder));
        var filesCount = Directory.GetFiles(folder, "*.asset").Length;
        AssetDatabase.CreateAsset(asset, folder + "IrisShotTween_" + filesCount + ".asset");
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }

    // unsubscribe from handler when script destroys
    private void OnDestroy()
    {
        if (postEffector)
            postEffector.TextureSizeChanged -= DoSizeProportionCorrection;
    }
}
