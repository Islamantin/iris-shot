﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(IrisShotController))]
public class SimpleIrisShotTweener : MonoBehaviour
{
    public bool autoStart = true;

    //public bool continueFromLastValues = true;

    public float tweenSpeedMultiplier = 1f;

    private IrisShotController controller;

    private IrisShotTween currentTween;

    private float timePointer = 0f;


    void Start()
    {
        controller = GetComponent<IrisShotController>();
    }

    public void SetTween(IrisShotTween newTween)
    {
        //if (!continueFromLastValues || currentTween == null)
            currentTween = newTween;
        //else
        //    currentTween = currentTween.MakeContinuingTween(newTween);
        if (autoStart)
            StartTweening();
    }

    public void StartTweening()
    {
        timePointer = 0f;
        StartCoroutine(DoTweening());
    }

    IEnumerator DoTweening()
    {
        while (timePointer < 1f && currentTween != null)
        {
            yield return new WaitForEndOfFrame();
            controller.Alpha = currentTween.alpha.Evaluate(timePointer);
            controller.Scale = currentTween.scale.Evaluate(timePointer);
            var posX = currentTween.positionX.Evaluate(timePointer);
            var posY = currentTween.positionY.Evaluate(timePointer);
            controller.Position = new Vector2(posX, posY);
            var sPropX = currentTween.sizeProportionX.Evaluate(timePointer);
            var sPropY = currentTween.sizeProportionY.Evaluate(timePointer);
            controller.Proportion = new Vector2(sPropX, sPropY);
            timePointer += Time.deltaTime * tweenSpeedMultiplier;
        }
    }
}



public class IrisShotTweenFactory
{
    //public static IrisShotTween Clone(IrisShotTween original)
    //{
    //    var clone = new IrisShotTween();
    //    clone.alpha = original.alpha;
    //    clone.scale = original.scale;
    //    clone.positionX = original.positionX;
    //    clone.positionY = original.positionY;
    //    clone.sizeProportionX = original.sizeProportionX;
    //    clone.sizeProportionY = original.sizeProportionY;
    //    return clone;
    //}


}

[System.Serializable]
public class IrisShotTween : ScriptableObject
{

    public AnimationCurve alpha = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

    public AnimationCurve scale = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

    public AnimationCurve positionX = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

    public AnimationCurve positionY = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

    public AnimationCurve sizeProportionX = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

    public AnimationCurve sizeProportionY = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

    public enum CurveName
    {
        alpha,
        scale,
        positionX,
        positionY,
        sizeProportionX,
        sizeProportionY
    }

    public IrisShotTween MakeContinuingTween(IrisShotTween next)
    {
        var result = ScriptableObject.CreateInstance<IrisShotTween>();
        foreach (var curveName
            in (IrisShotTween.CurveName[])Enum.GetValues(typeof(IrisShotTween.CurveName)))
        {
            var property = typeof(IrisShotTween).GetProperty(curveName.ToString(), typeof(AnimationCurve));
            Debug.Log(property);
            var thisTweenKeys = ((AnimationCurve)property.GetValue(this)).keys;
            var nextTweenKeys = ((AnimationCurve)property.GetValue(next)).keys;
            var curve = AnimationCurve.EaseInOut(
                0f, thisTweenKeys[0].value,
                1f, nextTweenKeys[nextTweenKeys.Length - 1].value);
            property.SetValue(result, curve);
        }
        return result;
    }
}
