﻿Shader "Custom/IrisShotShader"
{
    Properties
    {

        _MainTex ("Texture", 2D) = "white" {}
		_Alpha("Alpha", Range(0.0,1.0)) = 1.0
		_Scale("Scale",  Range(0.0,1.3)) = 0.0
		_CenterX("Center X", Range(0.0,1.0)) = 0.5
		_CenterY("Center Y", Range(0.0,1.0)) = 0.5
		_HorizontalProp("Horizontal Proportion", Range(0.0,1.0)) = 0.5
		_VerticalProp("Vertical Proportion", Range(0.0,1.0)) = 0.5
    }
    SubShader
    {
        Cull Off ZWrite Off ZTest Always

		Tags { "Queue" = "Transparent+1" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
			float _Scale;
			float _Alpha;
			float _CenterX;
			float _CenterY;
			float _HorizontalProp;
			float _VerticalProp;

            fixed4 frag (v2f i) : SV_Target
            {
				float r2 = pow((i.uv.x - _CenterX),2)*pow(_HorizontalProp,2) + pow((i.uv.y - _CenterY),2)*pow(_VerticalProp,2);
				float4 col = tex2D(_MainTex, i.uv.xy);
				if (r2 > pow(_Scale, 2)) {
					col = col * float4(_Alpha, _Alpha, _Alpha, _Alpha);
				}
				return col;
            }
            ENDCG
        }
    }
}
