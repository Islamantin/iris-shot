﻿using UnityEngine;
using System;

// Shortly, this script puts image from screen to material,
// where shader handles it, and then this new image returns back on screen 
[ExecuteInEditMode]
public class PostEffector : MonoBehaviour
{
    public Material material; // target material
    public Vector2 TextureSize { get; private set; } // width and hight of screen image

    private void Awake()
    {
        // just for safety
        if (Application.isEditor)
        {
            if (GetComponent<Camera>() == null)
            {
                Debug.LogError("Current GameObject has no Camera component on it." +
                    " Attach this script only to Camera.");
                GameObject.DestroyImmediate(this);
            }
        }
    }

    // MonoBehaviour method. Invokes each time when it needs to render image
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        var ts = new Vector2(source.width, source.height);
        // when screen size (image size) is changed, then event triggers
        if (!ts.Equals(TextureSize))
        {
            TextureSize = ts;
            var args = new TextureSizeChangedEventArgs();
            args.NewTextureSize = TextureSize;
            OnTextureChanged(args);
        }
        // does main stuff
        Graphics.Blit(source, destination, material);
    }

    // event triggering
    void OnTextureChanged(TextureSizeChangedEventArgs e)
    {
        var handler = TextureSizeChanged;
        if (handler!=null)
            handler.Invoke(this, e);
    }

    public event EventHandler<TextureSizeChangedEventArgs> TextureSizeChanged;
}

public class TextureSizeChangedEventArgs : EventArgs
{
    public Vector2 NewTextureSize { get; set; }
}
